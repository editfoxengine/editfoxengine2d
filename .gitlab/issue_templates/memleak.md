## Where?

(Which code segment causes the memory leak? example:)

In file example.cxx, starting at line 24:
```cpp
void leak() {
    void* dangling_ptr = malloc(1024);
}
```

## How does the memory leak affect the behaviour of the program?

(example:)

The leak causes the Sprite Rendering Pipeline to break, resulting in the Sprite vertex coordinate corruption.

## Memory usage graph

(Insert the memory usage graph showcasing the leak here)

## Possible Fix

Free the memory

/label ~bug ~memory_leak
/cc @Kuba663