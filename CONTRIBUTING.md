## Developer Certificate of Origin and License

By contributing to EditFoxEngine, you accept and agree to the following terms and
conditions for your present and future contributions submitted to GitLab B.V.
Except for the license granted herein to EditFoxEngine. and recipients of software
distributed by EditFoxEngine, you reserve all right, title, and interest in and to
your Contributions.

All contributions are subject to the Developer Certificate of Origin and license set out at [docs.gitlab.com/ce/legal/developer_certificate_of_origin](https://docs.gitlab.com/ce/legal/developer_certificate_of_origin).

## Contributing

Follow the rules:
1. Comment your code
    * Assembly code must provide a line by line explainanion of what it does in the comment
2. Document every function clearly.
    * Describe Every parameter and how does it affect the execution of the function
    * Summarize what happens inside the function
    * Describe what purpose does the returned value serve. In case of C-style error checking (the function returns the error/success) descrie what each error means.
    * Describe the meaining of each exception and provide a default method of handling it if such method exists.
3. Use exceptions specially made to handle specific cases. If an exception you think is needed does not exist petition the leadership to create it.
4. Use the macros
5. Try to write code that will compile on most (if not every) C/C++ compiler.
6. Use the C++ Attributes
    * On branches, use the \[\[likely\]\] and \[\[unlikely\]\] attributes in case you are sure that the branch has a more than 60% chance for execution (for \[\[likely]]) or less than 40% chance (for \[\[unlikely\]\]) of execution. In other cases do not use attributes.
    * If a function returns a pointer to newly allocated memory you __MUST__ use \[\[nodiscard\]\] or a macro providing thereof.
7. Use smart pointers.
8. Functions that can be constexpr or consteval should be constexpr or consteval.
9. Don't use singletons.
10. Don't use global variables.
11. Macros should be kept to macros.h (except the CLASS macro)
12. In C++ source files containing implementations of class members define a CLASS macro containing the name of the implemented class.
13. Do not commit directly to master.
14. Do not commit to other people's branches unless requested to do so.

## PR 101 or how to not get your changes rejected

1. Test your code.
2. Provide a graph of memory usage to proove that your contributions did not cause a memory leak.
3. Follow the contribution rules.
4. Provide proof your code works.
5. Write tests in case of implementation of new features.
6. In case of optimisation changes provide benchmarks against the older version.
7. Don't seethe at negative Code Reviews unless you are 100% sure it's a troll, then you have all the rights to do so.