EditFoxEngine2D
===============

EditFoxEngine2D is a Vulkan renderer for 2D video games. This is the core project.

## Dependencies

* Vulkan
* [Volk](https://github.com/zeux/volk.git)
* [VMA](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator.git)
* [GLFW](https://github.com/glfw/glfw.git)
* [GLM](https://github.com/g-truc/glm.git)

They are provided as submodules so there should be no problem with inclusion.

## Building

The project is built using CMake. 

TODO: Provide a command line snippet of the building procedure in this section.

## Installation

The binaries should be in Releases. Download them from there. If they are not that means the project isn't usable yet.

## Contributing

See CONTRIBUTING.md

## License

See LICENSE.md

## Roadmap
 - [ ] Sprite Rendering Pipeline
   - [ ] The actual pipeline code
   - [ ] Sprite Shaders
 - [ ] Tilemap Rendering Pipeline
   - [ ] The actual pipeline code
   - [ ] Tilemap Shaders
 - [ ] Partile Pipeline
   - [ ] The actual pipeline code
   - [ ] Particle Shader
 - [ ] Lighting Pipeline
   - [ ] Figuring out how the fuck will we implement this
   - [ ] Trying to implement the feature
   - [ ] Writing Shaders
   - [ ] Trying to make this work at a usable speed