#ifndef __EFE_PCH_
#define __EFE_PCH_

#include <volk.h>
#ifdef __cplusplus
#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>
#include <memory>
#include <frozen/string.h>
#endif

#define VK_NO_PROTOTYPES
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vk_mem_alloc.h>
#undef VK_NO_PROTOTYPES

#endif