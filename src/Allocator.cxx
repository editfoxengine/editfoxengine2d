#include "Allocator.hpp"

_USE(_EFE);


constexpr inline VmaVulkanFunctions vulkanFunctions() {
    VmaVulkanFunctions vma_vulkan_func{};
    vma_vulkan_func.vkAllocateMemory = vkAllocateMemory;
    vma_vulkan_func.vkBindBufferMemory = vkBindBufferMemory;
    vma_vulkan_func.vkBindImageMemory = vkBindImageMemory;
    vma_vulkan_func.vkCreateBuffer = vkCreateBuffer;
    vma_vulkan_func.vkCreateImage = vkCreateImage;
    vma_vulkan_func.vkDestroyBuffer = vkDestroyBuffer;
    vma_vulkan_func.vkDestroyImage = vkDestroyImage;
    vma_vulkan_func.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges;
    vma_vulkan_func.vkFreeMemory = vkFreeMemory;
    vma_vulkan_func.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements;
    vma_vulkan_func.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements;
    vma_vulkan_func.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
    vma_vulkan_func.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
    vma_vulkan_func.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges;
    vma_vulkan_func.vkMapMemory = vkMapMemory;
    vma_vulkan_func.vkUnmapMemory = vkUnmapMemory;
    vma_vulkan_func.vkCmdCopyBuffer = vkCmdCopyBuffer;
    vma_vulkan_func.vkGetDeviceProcAddr = vkGetDeviceProcAddr;
    vma_vulkan_func.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
    return vma_vulkan_func;
}

#ifndef NDEBUG
static VKAPI_ATTR void VKAPI_PTR allocationCallback(VmaAllocator allocator, uint32_t memoryType, VkDeviceMemory memory, VkDeviceSize size, void* pUserData) {
    std::clog << std::format("Freed {} type {} bytes", size, memoryType) << std::endl;
}

static VKAPI_ATTR void VKAPI_PTR freeCallback(VmaAllocator allocator, uint32_t memoryType, VkDeviceMemory memory, VkDeviceSize size, void* pUserData) {
    std::clog << std::format("Allocated {} bytes of {} type memory", size, memoryType) << std::endl;
}
#endif // !NDEBUG


EFE_API Allocator::Allocator(std::weak_ptr<Device> _device)
{
	auto device = _device.lock();
	VmaAllocatorCreateInfo info{};
	info.instance = volkGetLoadedInstance();
	info.device = volkGetLoadedDevice();
	info.vulkanApiVersion = VK_API_VERSION_1_3;
	info.physicalDevice = device->physicalDevice;
    auto f = vulkanFunctions();
    info.pVulkanFunctions = &f;
    info.flags = VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT | VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;
#ifndef NDEBUG
    const VmaDeviceMemoryCallbacks cbMemory = { allocationCallback, freeCallback, nullptr };
    info.pDeviceMemoryCallbacks = &cbMemory;
#endif // !NDEBUG
    VK_ASSERT(vmaCreateAllocator(&info, &this->_allocator), "Couldn't create allocator");
}

EFE_API Allocator::~Allocator()
{
    vmaDestroyAllocator(this->_allocator);
}

VmaPool EFE_API Allocator::createPool(VkDeviceSize blockSize, VkDeviceSize blocks, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBufferCreateFlags flags, VmaMemoryUsage _usage) const&
{
    VmaPool pool;
    VmaPoolCreateInfo info{};
    info.blockSize = blockSize;
    info.maxBlockCount = blocks;
    VkBufferCreateInfo sampleBuff{ .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    sampleBuff.usage = usage;
    sampleBuff.flags = flags;
    sampleBuff.size = blockSize;
    VmaAllocationCreateInfo sampleMemory{};
    sampleMemory.usage = _usage;
    sampleMemory.requiredFlags = properties;
    VK_ASSERT(vmaFindMemoryTypeIndexForBufferInfo(this->_allocator, &sampleBuff, &sampleMemory, &info.memoryTypeIndex), "Couldn't find memory type index for buffer");
    VK_ASSERT(vmaCreatePool(this->_allocator, &info, &pool), "Couldn't create pool");
    return pool;
}

VmaPool EFE_API Allocator::createPool(VkDeviceSize blockSize, VkDeviceSize blocks, const VkImageCreateInfo& imageInfo, VmaMemoryUsage _usage) const&
{
    VmaPool pool;
    VmaPoolCreateInfo info{};
    info.blockSize = blockSize;
    info.maxBlockCount = blocks;
    VmaAllocationCreateInfo sampleMemory{};
    sampleMemory.usage = _usage;
    VK_ASSERT(vmaFindMemoryTypeIndexForImageInfo(this->_allocator, &imageInfo, &sampleMemory, &info.memoryTypeIndex), "Couldn't find memory type index for image");
    VK_ASSERT(vmaCreatePool(this->_allocator, &info, &pool), "Couldn't create pool");
    return pool;
}

Allocator::Allocation EFE_API Allocator::allocateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBufferCreateFlags flags, VmaMemoryUsage _usage) const&
{
    Allocator::Allocation alloc{ Allocator::Allocation::BUFFER, *this };
    VkBufferCreateInfo info{ .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    info.size = size;
    info.usage = usage;
    info.flags = flags;
    VmaAllocationCreateInfo memInfo{};
    memInfo.requiredFlags = properties;
    VK_ASSERT(vmaFindMemoryTypeIndexForBufferInfo(this->_allocator, &info, &memInfo, &memInfo.memoryTypeBits), "Couldn't find memory type index for buffer");
    VK_ASSERT(vmaCreateBuffer(this->_allocator, &info, &memInfo, &alloc.buffer, &alloc.allocation, nullptr), "Couldn't create buffer");
    return alloc;
}

Allocator::Allocation EFE_API Allocator::allocateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkBufferCreateFlags flags, VmaPool& pool) const&
{
    Allocator::Allocation alloc{ Allocator::Allocation::BUFFER, *this };
    VkBufferCreateInfo info{ .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    info.size = size;
    info.usage = usage;
    info.flags = flags;
    VmaAllocationCreateInfo memInfo{};
    memInfo.pool = pool;
    VK_ASSERT(vmaCreateBuffer(this->_allocator, &info, &memInfo, &alloc.buffer, &alloc.allocation, nullptr), "Couldn't create buffer");
    return alloc;
}

Allocator::Allocation EFE_API Allocator::allocateImage(VkDeviceSize size, const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VmaMemoryUsage _usage) const&
{
    Allocator::Allocation alloc{ Allocator::Allocation::IMAGE, *this };
    VmaAllocationCreateInfo memInfo{};
    memInfo.usage = _usage;
    memInfo.requiredFlags = properties;
    VK_ASSERT(vmaFindMemoryTypeIndexForImageInfo(this->_allocator, &imageInfo, &memInfo, &memInfo.memoryTypeBits), "Couldn't find memory type index for image");
    VK_ASSERT(vmaCreateImage(this->_allocator, &imageInfo, &memInfo, &alloc.image, &alloc.allocation, nullptr), "Couldn't create image");
    return alloc;
}

Allocator::Allocation EFE_API Allocator::allocateImage(VkDeviceSize size, const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VmaPool& pool) const&
{
    Allocator::Allocation alloc{ Allocator::Allocation::IMAGE, *this };
    VmaAllocationCreateInfo memInfo{};
    memInfo.pool = pool;
    VK_ASSERT(vmaCreateImage(this->_allocator, &imageInfo, &memInfo, &alloc.image, &alloc.allocation, nullptr), "Couldn't create image");
    return alloc;
}



void EFE_API Allocator::free(Allocation* memory) const& {
	switch (memory->type) {
	case Allocation::BUFFER:
		vmaDestroyBuffer(this->_allocator, memory->buffer, memory->allocation);
		break;
	case Allocation::IMAGE:
		vmaDestroyImage(this->_allocator, memory->image, memory->allocation);
		break;
	}
}

Allocator::Allocation::Allocation(Allocation::_Type type, const Allocator& alloc)
    :type{type}, _alloc{alloc}
{
}

Allocator::Allocation::Allocation(Allocation& from)
    :_alloc{from._alloc}, type{from.type}
{
    *this = std::move(from);
}

EFE_API Allocator::Allocation::~Allocation()
{
	this->_alloc.free(this);
}

Allocator::Allocation& EFE_API Allocator::Allocation::operator=(const Allocator::Allocation&& from)
{
    if (from.type == this->type) {
        this->allocation = from.allocation;
        switch (from.type) {
        case Allocation::BUFFER:
            this->buffer = from.buffer;
            break;
        case Allocation::IMAGE:
            this->image = from.image;
            break;
        }
    }
    else throw new std::runtime_error("Allocation type is not the same");
    return *this;
}
