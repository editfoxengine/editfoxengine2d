#ifndef __EFE_SWAPCHAIN_H_
#define __EFE_SWAPCHAIN_H_

#include "macros.h"
#include "pch.h"
#include "Engine.hpp"
#include "Device.hpp"

#include <array>

_EFE
_BEGIN

class Engine;
class Device;

class SwapChain {
public:
	static constexpr int MAX_FRAMES_IN_FLIGHT = 2;

	EFE_API SwapChain(std::weak_ptr<Device> deviceRef, VkExtent2D windowExtent);
	EFE_API ~SwapChain();

	SwapChain(const SwapChain&) = delete;
	void operator=(const SwapChain&) = delete;

	inline VkFramebuffer getFrameBuffer(int index) { return swapChainFramebuffers[index]; }
	inline VkRenderPass getRenderPass() { return renderPass; }
	inline VkImageView getImageView(int index) { return swapChainImageViews[index]; }
	inline size_t imageCount() { return swapChainImages.size(); }
	inline VkFormat getSwapChainImageFormat() { return swapChainImageFormat; }
	inline VkExtent2D getSwapChainExtent() { return swapChainExtent; }
	inline uint32_t width() { return swapChainExtent.width; }
	inline uint32_t height() { return swapChainExtent.height; }

	inline float extentAspectRatio() {
		return static_cast<float>(swapChainExtent.width) / static_cast<float>(swapChainExtent.height);
	}
private:
	void createSwapChain();
	void createImageViews();
	void createRenderPass();
	void createFramebuffers();
	void createSyncObjects();

	// Helper functions
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

	std::vector<VkFramebuffer> swapChainFramebuffers;
	VkRenderPass renderPass;

	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;

	std::weak_ptr<Device> _device;
	VkExtent2D windowExtent;

	VkSwapchainKHR swapChain;

	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	std::vector<VkFence> imagesInFlight;
	size_t currentFrame = 0;
};

_END

#endif