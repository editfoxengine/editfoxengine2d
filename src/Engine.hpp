#ifndef __EFE_ENGINE_H_
#define __EFE_ENGINE_H_

#include "macros.h"
#include "pch.h"
#include "Window.hpp"
#include "Device.hpp"
#include "SwapChain.hpp"
#include "Allocator.hpp"

_EFE
_BEGIN

class Device;
class Window;
class SwapChain;
class Allocator;

/*
 The only class allowed to host "global" variables
*/
class Engine {
public:
	explicit EFE_API Engine(std::string&& appName, int&& major, int&& minor, int&& patch);
	EFE_API ~Engine();
	
	Engine(const Engine&) = delete;
	void operator=(const Engine&) = delete;


	inline const VkInstance& instance() const& {
		return this->vkInstance;
	}

	inline const std::unique_ptr<Window>& window() const& {
		return this->_window;
	}

	inline const std::weak_ptr<Allocator> alocator() const& {
		return this->_allocator;
	}

private:

	VkInstance vkInstance;
	std::unique_ptr<Window> _window;
	VkDebugUtilsMessengerEXT debugMessenger;
	std::shared_ptr<Device> device;
	std::shared_ptr<SwapChain> swapChain;
	std::shared_ptr<Allocator> _allocator;

	inline std::vector<const char*> getRequiredExtensions() {
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

#ifndef NDEBUG
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif

		return extensions;
	}
};

_END

#endif