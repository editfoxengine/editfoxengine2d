#ifndef __EFE_WINDOW_H_
#define __EFE_WINDOW_H_

#include "macros.h"
#include "pch.h"
#include "Engine.hpp"

_EFE
_BEGIN

class Engine;

class Window {
public:
	EFE_API Window(std::string title, unsigned int width = 1080, unsigned int height = 720);
	EFE_API ~Window();

	Window(const Window&) = delete;
	void operator=(const Window&) = delete;

	inline void createWindowSurface(VkInstance instance, VkSurfaceKHR* surface) const& {
		VK_ASSERT(glfwCreateWindowSurface(instance, this->window, nullptr, surface), "Nie uda�o si� utworzy� przestrzeni okna");
	}
	const unsigned int width, height;
private:
	GLFWwindow* window;
};

_END

#endif