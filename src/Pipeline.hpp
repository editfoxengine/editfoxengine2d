#ifndef __EFE_PIPELINE_H_
#define __EFE_PIPELINE_H_

#include "macros.h"
#include "pch.h"

#include "Shader.hpp"
#include "Engine.hpp"
#include "Allocator.hpp"
#include "Sprite.hpp"

_EFE
_BEGIN

class Engine;
class Allocator;

struct PipelineConfigInfo {
	PipelineConfigInfo() = default;
	PipelineConfigInfo(const PipelineConfigInfo&) = delete;
	PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;

	VkViewport viewport;
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
	VkRect2D scissor;
	VkPipelineRasterizationStateCreateInfo rasterizationInfo;
	VkPipelineMultisampleStateCreateInfo multisampleInfo;
	VkPipelineColorBlendAttachmentState colorBlendAttachment;
	VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
	VkPipelineLayout pipelineLayout = nullptr;
	VkRenderPass renderPass = nullptr;
	uint32_t subpass = 0;
};

class SpritePipeline {
public:
	EFE_API SpritePipeline(const Engine& engine, std::string vertShader, std::string fragShader);
	EFE_API ~SpritePipeline();

	static constexpr VkDeviceSize indirectOffset = 0;
	static constexpr VkDeviceSize vertexOffset = sizeof(VkDrawIndirectCommand) * MAX_SPRITES;
	static constexpr VkDeviceSize indexOffset = vertexOffset + sizeof(glm::vec2) * 4;
	static constexpr VkDeviceSize ssbOffset = indexOffset + 4;
	static constexpr VkDeviceSize buffSize = ssbOffset + sizeof(Sprite) * MAX_SPRITES;
private:
	const uint32_t width, height;
	inline VkPipelineViewportStateCreateInfo getViewportStateCreateInfo(PipelineConfigInfo& info) {
		VkPipelineViewportStateCreateInfo viewportInfo{};
		viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportInfo.viewportCount = 1;
		viewportInfo.pViewports = &info.viewport;
		viewportInfo.scissorCount = 1;
		viewportInfo.pScissors = &info.scissor;
		return viewportInfo;
	}
	inline VkPipelineColorBlendStateCreateInfo getColorBlendStateCreateInfo(PipelineConfigInfo& info) {
		VkPipelineColorBlendStateCreateInfo colorBlendInfo{};
		colorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlendInfo.logicOpEnable = VK_FALSE;
		colorBlendInfo.logicOp = VK_LOGIC_OP_COPY;  // Optional
		colorBlendInfo.attachmentCount = 1;
		colorBlendInfo.pAttachments = &info.colorBlendAttachment;
		colorBlendInfo.blendConstants[0] = 0.0f;  // Optional
		colorBlendInfo.blendConstants[1] = 0.0f;  // Optional
		colorBlendInfo.blendConstants[2] = 0.0f;  // Optional
		colorBlendInfo.blendConstants[3] = 0.0f;  // Optional
		return colorBlendInfo;
	}

	inline void createPipeline() {
#pragma region DescriptorSets
		VkDescriptorSetLayoutBinding ssboLayoutBinding{};
		ssboLayoutBinding.binding = 0;
		ssboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		ssboLayoutBinding.descriptorCount = sprites.size();
		ssboLayoutBinding.pImmutableSamplers = VK_NULL_HANDLE;
		ssboLayoutBinding.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;
		VkDescriptorSetLayoutCreateInfo dsCreateInfo{ .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
		dsCreateInfo.bindingCount = 1;
		dsCreateInfo.pBindings = &ssboLayoutBinding;
		dsCreateInfo.pNext = VK_NULL_HANDLE;
		dsCreateInfo.flags = 0;
		VK_ASSERT(vkCreateDescriptorSetLayout(volkGetLoadedDevice(), &dsCreateInfo, nullptr, &this->descriptorSetLayout), "Couldn't create descriptor set layout");
#pragma endregion
#pragma region PipelineConfigInfo
		PipelineConfigInfo config{};
		config.inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		config.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
		config.inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

		config.viewport.x = 0.0f;
		config.viewport.y = 0.0f;
		config.viewport.width = static_cast<float>(width);
		config.viewport.height = static_cast<float>(height);
		config.viewport.minDepth = 0.0f;
		config.viewport.maxDepth = 1.0f;

		config.scissor.offset = { 0, 0 };
		config.scissor.extent = { width, height };

		config.rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		config.rasterizationInfo.depthClampEnable = VK_FALSE;
		config.rasterizationInfo.rasterizerDiscardEnable = VK_FALSE;
		config.rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
		config.rasterizationInfo.lineWidth = 1.0f;
		config.rasterizationInfo.cullMode = VK_CULL_MODE_NONE;
		config.rasterizationInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
		config.rasterizationInfo.depthBiasEnable = VK_FALSE;
		config.rasterizationInfo.depthBiasConstantFactor = 0.0f;  // Optional
		config.rasterizationInfo.depthBiasClamp = 0.0f;           // Optional
		config.rasterizationInfo.depthBiasSlopeFactor = 0.0f;     // Optional

		config.multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		config.multisampleInfo.sampleShadingEnable = VK_FALSE;
		config.multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		config.multisampleInfo.minSampleShading = 1.0f;           // Optional
		config.multisampleInfo.pSampleMask = nullptr;             // Optional
		config.multisampleInfo.alphaToCoverageEnable = VK_FALSE;  // Optional
		config.multisampleInfo.alphaToOneEnable = VK_FALSE;       // Optional

		config.colorBlendAttachment.colorWriteMask =
			VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
			VK_COLOR_COMPONENT_A_BIT;
		config.colorBlendAttachment.blendEnable = VK_FALSE;
		config.colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
		config.colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
		config.colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;              // Optional
		config.colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;   // Optional
		config.colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
		config.colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;              // Optional

		config.depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		config.depthStencilInfo.depthTestEnable = VK_FALSE;
		config.depthStencilInfo.depthWriteEnable = VK_FALSE;
		config.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
		config.depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
		config.depthStencilInfo.minDepthBounds = 0.0f;  // Optional
		config.depthStencilInfo.maxDepthBounds = 1.0f;  // Optional
		config.depthStencilInfo.stencilTestEnable = VK_FALSE;
		config.depthStencilInfo.front = {};  // Optional
		config.depthStencilInfo.back = {};   // Optional

		VkPipelineLayoutCreateInfo pipelineLayoutInfo{ .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
		VK_ASSERT(vkCreatePipelineLayout(volkGetLoadedDevice(), &pipelineLayoutInfo, nullptr, &config.pipelineLayout), "Couldn't create pipeline layout");
#pragma endregion
#pragma region PipelineInfo
		VkPipelineShaderStageCreateInfo shaderStages[2];
		shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
		shaderStages[0].module = vertex.module();
		shaderStages[0].pName = "main";
		shaderStages[0].flags = 0;
		shaderStages[0].pNext = nullptr;
		shaderStages[0].pSpecializationInfo = nullptr;
		shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		shaderStages[1].module = fragment.module();
		shaderStages[1].pName = "main";
		shaderStages[1].flags = 0;
		shaderStages[1].pNext = nullptr;
		shaderStages[1].pSpecializationInfo = nullptr;

		VkVertexInputBindingDescription bindings[] = { {0, sizeof(glm::vec2), VK_VERTEX_INPUT_RATE_VERTEX } };
		VkVertexInputAttributeDescription attributes[] = { { 0, 0, VK_FORMAT_R32G32_SFLOAT, 0 } };
		VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputInfo.vertexAttributeDescriptionCount = 1;
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.pVertexAttributeDescriptions = attributes;
		vertexInputInfo.pVertexBindingDescriptions = bindings;

		VkGraphicsPipelineCreateInfo pipelineInfo{};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &config.inputAssemblyInfo;
		pipelineInfo.pViewportState = &getViewportStateCreateInfo(config);
		pipelineInfo.pRasterizationState = &config.rasterizationInfo;
		pipelineInfo.pColorBlendState = &getColorBlendStateCreateInfo(config);
		pipelineInfo.pDepthStencilState = &config.depthStencilInfo;
		pipelineInfo.pDynamicState = nullptr;

		pipelineInfo.layout = config.pipelineLayout;
		pipelineInfo.renderPass = config.renderPass;
		pipelineInfo.subpass = config.subpass;

		pipelineInfo.basePipelineIndex = -1;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
#pragma endregion
		VK_ASSERT(vkCreateGraphicsPipelines(volkGetLoadedDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, VK_NULL_HANDLE, &pipeline), "Couldn't create pipeline");
	}

	VkPipeline pipeline;
	VkDescriptorSetLayout descriptorSetLayout;

	Shader vertex;
	Shader fragment;	

	std::unique_ptr<Allocator::Allocation> staging;
	std::unique_ptr<Allocator::Allocation> buffer;

	std::vector<Sprite> sprites;

	const glm::vec2 mesh[4] = { {0.0f, 0.0f}, {0.0f, 1.0f}, { 1.0f, 1.0f }, { 1.0f, 0.0f } };
	const uint_fast8_t index[4] = { 0, 1, 2, 3 };
};

_END

#endif