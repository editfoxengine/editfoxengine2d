#ifndef __EFE_ALLOCATOR_H_
#define __EFE_ALLOCATOR_H_

#include "macros.h"
#include "pch.h"
#include "Device.hpp"

#include <format>

_EFE
_BEGIN

class Device;

class Allocator {
public:

	struct Allocation {
		const enum _Type {
			BUFFER,
			IMAGE
		} type;
		union {
			VkBuffer buffer;
			VkImage image;
		};
		VmaAllocation allocation;
		const Allocator& _alloc;

		EFE_API Allocation(Allocation::_Type type, const Allocator& alloc);
		EFE_API Allocation(Allocation&);
		EFE_API ~Allocation();
		Allocation& EFE_API operator=(const Allocation&&);
	};

	explicit EFE_API Allocator(std::weak_ptr<Device> device);
	EFE_API ~Allocator();

	[[nodiscard]] VmaPool EFE_API createPool(VkDeviceSize blockSize, VkDeviceSize blocks, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBufferCreateFlags flags, VmaMemoryUsage _usage = VMA_MEMORY_USAGE_AUTO) const&;
	[[nodiscard]] VmaPool EFE_API createPool(VkDeviceSize blockSize, VkDeviceSize blocks, const VkImageCreateInfo& imageInfo, VmaMemoryUsage _usage = VMA_MEMORY_USAGE_AUTO) const&;
	[[nodiscard]] Allocation EFE_API allocateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBufferCreateFlags flags, VmaMemoryUsage _usage = VMA_MEMORY_USAGE_AUTO) const&;
	[[nodiscard]] Allocation EFE_API allocateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkBufferCreateFlags flags, VmaPool& pool) const&;
	[[nodiscard]] Allocation EFE_API allocateImage(VkDeviceSize size, const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VmaMemoryUsage _usage = VMA_MEMORY_USAGE_AUTO) const&;
	[[nodiscard]] Allocation EFE_API allocateImage(VkDeviceSize size, const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VmaPool& pool) const&;
	void EFE_API free(Allocation* memory) const&;

	friend class EFE::Device;
private:

	VmaAllocator _allocator;
};

_END


#endif