#include "Window.hpp"

_USE(_EFE);

Window::Window(std::string title, unsigned int width, unsigned int height)
	:width(width), height(height)
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	this->window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
}

Window::~Window()
{
	glfwDestroyWindow(this->window);
	glfwTerminate();
}
