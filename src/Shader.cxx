#include "Shader.hpp"

_USE(_EFE);

Shader::Shader(std::string fname, Type type)
	: type(type)
{
	auto data = loadFile(fname);
	VkShaderModuleCreateInfo info{ .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO };
	info.codeSize = data.size();
	info.pCode = reinterpret_cast<const uint32_t*>(data.data());
	VK_ASSERT(vkCreateShaderModule(volkGetLoadedDevice(), &info, nullptr, &this->_module), "Couldn't create shader module!");
}

Shader::~Shader()
{
	vkDestroyShaderModule(volkGetLoadedDevice(), this->_module, nullptr);
}

std::vector<char> Shader::loadFile(std::string fname)
{
	std::ifstream file{ fname, std::ios::ate | std::ios::binary };

	if (!file.is_open()) [[unlikely]] throw new std::runtime_error("Couldn't open file " + fname);

	size_t fileSize = static_cast<size_t>(file.tellg());
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}
