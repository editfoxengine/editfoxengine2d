#ifndef __EFE_DEVICE_H_
#define __EFE_DEVICE_H_

#include "macros.h"
#include "pch.h"
#include "Engine.hpp"
#include <set>

_EFE
_BEGIN

class Engine;
class Window;
class SwapChain;
class Allocator;

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

struct QueueFamilyIndices {
	uint32_t graphicsFamily : 32;
	uint32_t presentFamily : 32;
	bool graphicsFamilyHasValue : 1 = false;
	bool presentFamilyHasValue : 1 = false;
	bool isComplete() { return graphicsFamilyHasValue && presentFamilyHasValue; }
};

class Device {
	Engine& engine;
public:
	explicit EFE_API Device(Engine& engine);
	EFE_API ~Device();

	Device(const Device&) = delete;
	void operator=(const Device&) = delete;
	Device(Device&&) = delete;
	Device& operator=(Device&&) = delete;

	inline SwapChainSupportDetails getSwapChainSupport() { return querySwapChainSupport(physicalDevice); }
	inline QueueFamilyIndices findPhysicalQueueFamilies() { return findQueueFamilies(physicalDevice); }

	VkPhysicalDeviceProperties properties;

	friend class SwapChain;
	friend class Allocator;
private:

	void pickPhysicalDevice();
	void createLogicalDevice();
	void createCommandPool();

	VkDevice _device;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VkSurfaceKHR surface;
	VkQueue graphicsQueue_;
	VkQueue presentQueue_;

	VkCommandPool commandPool;

	// helper functions
	bool isDeviceSuitable(VkPhysicalDevice device);
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);

	constexpr static const char* extensions[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_KHR_8BIT_STORAGE_EXTENSION_NAME, VK_KHR_SHADER_FLOAT16_INT8_EXTENSION_NAME, VK_KHR_STORAGE_BUFFER_STORAGE_CLASS_EXTENSION_NAME,
VK_KHR_SHADER_CLOCK_EXTENSION_NAME, VK_KHR_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_EXTENSION_NAME, VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME, VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME,
VK_EXT_MEMORY_BUDGET_EXTENSION_NAME, VK_EXT_INDEX_TYPE_UINT8_EXTENSION_NAME, VK_KHR_BIND_MEMORY_2_EXTENSION_NAME, VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME};
};

_END

#endif