#ifndef __EFE_SPRITE_H_
#define __EFE_SPRITE_H_

#include "macros.h"
#include "pch.h"
#include <glm.hpp>

_EFE
_BEGIN

struct Sprite {
	glm::vec2 pos;
	uint8_t width;
	uint8_t height;
	glm::mat2x2 texCoords, normalCoords;
	uint8_t r, g, b, a;
};

_END

#endif