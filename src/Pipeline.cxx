#include "Pipeline.hpp"

_USE(_EFE);

EFE_API SpritePipeline::SpritePipeline(const Engine& engine, std::string vertShader, std::string fragShader)
    :vertex(vertShader, Shader::VERTEX), fragment(fragShader, Shader::FRAGMENT), width(engine.window()->width), height(engine.window()->height)
{
    auto allocator = engine.alocator().lock();
    this->staging = std::make_unique<Allocator::Allocation>(allocator->allocateBuffer(buffSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 0, VMA_MEMORY_USAGE_CPU_COPY));
    this->buffer = std::make_unique<Allocator::Allocation>(allocator->allocateBuffer(buffSize,
        VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        0, VMA_MEMORY_USAGE_GPU_ONLY));
    createPipeline();
}

SpritePipeline::~SpritePipeline()
{
    vkDestroyDescriptorSetLayout(volkGetLoadedDevice(), this->descriptorSetLayout, VK_NULL_HANDLE);
    vkDestroyPipeline(volkGetLoadedDevice(), this->pipeline, nullptr);
}