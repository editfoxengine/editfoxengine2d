#include "Engine.hpp"

_USE(_EFE);

const char* validationLayers[] = { "VK_LAYER_KHRONOS_validation" };

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData) {
    std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

    return VK_FALSE;
}

VkResult CreateDebugUtilsMessengerEXT(
    VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDebugUtilsMessengerEXT* pDebugMessenger) {
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
        instance,
        "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    }
    else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void DestroyDebugUtilsMessengerEXT(
    VkInstance instance,
    VkDebugUtilsMessengerEXT debugMessenger,
    const VkAllocationCallbacks* pAllocator) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
        instance,
        "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, pAllocator);
    }
}

inline void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = debugCallback;
    createInfo.pUserData = nullptr;  // Optional
}

Engine::Engine(std::string&& appName, int&& major, int&& minor, int&& patch)
    :_window(std::make_unique<Window>(appName))
{
#ifndef WINDOWS
    volkInitialize();
#endif
	VkInstanceCreateInfo info{ .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };

	VkApplicationInfo app{ .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO };
	app.pApplicationName = appName.c_str();
	app.applicationVersion = VK_MAKE_VERSION(major, minor, patch);
	app.apiVersion = VK_API_VERSION_1_2;
	app.engineVersion = VK_MAKE_VERSION(0, 0, 1);
	app.pEngineName = "EditFoxEngine";
	info.pApplicationInfo = &app;

	auto extensions = getRequiredExtensions();
	info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	info.ppEnabledExtensionNames = extensions.data();

#ifndef NDEBUG
	VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    info.enabledLayerCount = 1;
    info.ppEnabledLayerNames = validationLayers;
    populateDebugMessengerCreateInfo(debugCreateInfo);
    info.pNext = &debugCreateInfo;
#else
    info.enabledLayerCount = 0;
#endif

	VK_ASSERT(vkCreateInstance(&info, nullptr, &this->vkInstance), "Couldn't create instance");

    volkLoadInstance(this->vkInstance);

#ifndef NDEBUG
    VK_ASSERT(CreateDebugUtilsMessengerEXT(this->vkInstance, &debugCreateInfo, nullptr, &this->debugMessenger), "Couldn't create debug messenger!");
#endif

    this->device = std::make_shared<Device>(*this);
    
    this->swapChain = std::make_shared<SwapChain>(this->device, VkExtent2D{1080, 720});
}

Engine::~Engine()
{
#ifndef NDEBUG
    DestroyDebugUtilsMessengerEXT(this->vkInstance, this->debugMessenger, nullptr);
#endif
    vkDestroyInstance(this->vkInstance, nullptr);
#ifndef WINDOWS
    volkFinalize();
#endif
}