#ifndef __EFE_SHADER_H_
#define __EFE_SHADER_H_

#include "macros.h"
#include "pch.h"

#include <fstream>
#include <frozen/map.h>

_EFE
_BEGIN

class Shader {
public:
	using Type = enum {
		VERTEX,
		FRAGMENT,
		COMPUTE
	};

	explicit Shader(std::string fname)
		: Shader(fname, getTypeFromFName(frozen::string(fname)))
	{
	}
	EFE_API Shader(std::string fname, Type type);
	EFE_API ~Shader();

	inline VkShaderModule& module() { return _module; }

	const Type type;
protected:
	static [[nodiscard]] std::vector<char> EFE_API loadFile(std::string fname);
	
	constexpr static Type getTypeFromFName(frozen::string fname) {
		constexpr auto map = frozen::make_map<char, Shader::Type>({ {'t', Type::VERTEX}, {'g', Type::FRAGMENT}, {'p', Type::COMPUTE} });
		return map.at(fname[fname.size() - 4]);
	}

	VkShaderModule _module;
	

};

_END


#endif