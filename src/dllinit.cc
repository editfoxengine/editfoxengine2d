#include "macros.h"
#include "pch.h"
#ifdef WINDOWS

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        if (volkInitialize() != VK_SUCCESS) return FALSE;
        break;

    case DLL_THREAD_ATTACH:
        
        break;

    case DLL_THREAD_DETACH:
        
        break;

    case DLL_PROCESS_DETACH:
        volkFinalize();
        break;
    }
	return TRUE;
}

#endif