#ifndef __EFE_MACROS_H_
#define __EFE_MACROS_H_

#define EFE EditFoxEngine

#ifdef EFE_EXPORT
#define EFE_API __declspec(dllexport)
#else
#define EFE_API __declspec(dllimport)
#endif

#ifdef __cplusplus
#define _NS(ns) namespace ns
#define _EFE _NS(EFE) 
#define _VK_ASSERT(check, exception, msg) if (check != VK_SUCCESS) [[unlikely]] throw exception(msg);
#define _USE(x) using x
#define VK_ASSERT(check, msg) _VK_ASSERT(check, std::runtime_error, msg)
#endif

#define _BEGIN {
#define _END }

#endif